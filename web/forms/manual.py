from wtforms import Form, StringField, validators, SelectField, TextAreaField

class ManualFlowDataForm(Form):
    point = SelectField('Точка', [validators.DataRequired()], choices = [('1', '1'), ('2', '2'), ('3', '3'), ('4', '4') ,('5', '5')])
    session = SelectField('Сессия')

    preschoolers = TextAreaField('Дошкольники, начальная школа')
    y1017 = TextAreaField('Подростки 10-17')
    cadets = TextAreaField('Курсанты')
    students = TextAreaField('Студенты')
    employeed = TextAreaField('Занятые')
    parent_children = TextAreaField('Родители с детьми')
    pensioners = TextAreaField('Пенсионеры')