from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import timezone
import log

mongo_client = MongoClient("mongodb://mongo:27017")
db = mongo_client.traffic

def to_local_tz(dt):
    return dt.replace(tzinfo=timezone.utc).astimezone(tz=None)

class FlowSession:
    def __init__(self, id, date_start, date_finish):
        self.id = id
        self.date_start = date_start
        self.date_finish = date_finish
        self.points = []

    @staticmethod
    def from_db_object(db_session):
        return FlowSession(str(db_session['_id']), to_local_tz(db_session['date_start']), to_local_tz(db_session['date_finish']))

    def add_point(self, point):
        self.points.append(point)

    def _get_points_data(self, by_destination):
        points = {}
        for p in self.points:
            points[p.id] = p.get_data(by_destination)
        return points

    def _get_description(self):
        return '{} -- {}'.format(self.date_start.strftime("/%A/%d.%m.%Y [%H:%M]"), self.date_finish.strftime("/%A/%d.%m.%Y [%H:%M]"))

    def get_data_by_category(self):
        return {
            'description': self._get_description(),
            'points': self._get_points_data(False),
            'id': self.id
        }

    def get_data_by_destination(self):
        log.log_debug(self.points, "points")
        return {
            'description': self._get_description(),
            'points': self._get_points_data(True),
            'id': self.id
        }

class FlowPoint:
    def __init__(self, id, lat, lon):
        self.id = id
        self.lat = lat
        self.lon = lon
        self.directions = []

    def add_direction(self, direction):
        self.directions.append(direction)

    def _get_directions_by_category(self):
        data_by_cats = {}
        for d in self.directions:
            if d.category not in data_by_cats:
                data_by_cats[d.category] = []
            data_by_cats[d.category].append(str(d))
        return data_by_cats

    def _get_quantity_by_destination(self):
        data_by_dest = {}
        for d in self.directions:
            if d.dst not in data_by_dest:
                data_by_dest[d.dst] = {}
            if d.category not in data_by_dest[d.dst]:
                data_by_dest[d.dst][d.category] = 0
            data_by_dest[d.dst][d.category] += 1
        return data_by_dest

    def get_data(self, by_destination = False):
        return {
            'directions': self._get_quantity_by_destination() if by_destination else self._get_directions_by_category(),
            'lat': self.lat,
            'lon': self.lon
        }

class FlowDirection:
    def __init__(self, src, dst, category):
        self.src = src
        self.dst = dst
        self.category = category

    def __str__(self):
        return self.src + self.dst

    @staticmethod
    def as_string(str_dir, category):
        return FlowDirection(str_dir[0], str_dir[1], category)

    @staticmethod
    def as_db_object(db_direction):
        return FlowDirection.as_string(db_direction['direction'], db_direction['category'])

def get_local_dt(dt):
    return dt.replace(tzinfo=timezone.utc).astimezone(tz=None).strftime("/%A/%d.%m.%Y [%H:%M]")

def get_session_choice(db_session):
    return (str(db_session.get('_id')), "{} - {}".format(get_local_dt(db_session['date_start']), get_local_dt(db_session['date_finish'])))

def get_sessions_choices():
    return list(map(get_session_choice, db.sessions.find()))

def add_points_to_session(session, point_set_id):  
    point_set = db.point_sets.find_one({'_id': point_set_id})
    for point in point_set['points']:
        point = FlowPoint(point['_id'], point['lat'], point['lon'])
        add_directions_to_point(point, session)
        session.add_point(point)

def add_directions_to_point(point, session):
    for d in db.directions.find({'session_id': ObjectId(session.id), 'point_id':point.id}):
        point.add_direction(FlowDirection.as_db_object(d))

def get_session_by_id(session_id):
    db_session = db.sessions.find_one({'_id': ObjectId(session_id)})
    session = FlowSession(str(db_session['_id']), db_session['date_start'], db_session['date_finish'])
    add_points_to_session(session, db_session['point_set'])
    return session

def get_all_sessions(by_directions = False):
    sessions = []
    for s in db.sessions.find():
        fs = FlowSession.from_db_object(s)
        add_points_to_session(fs, s['point_set'])
        sessions.append(fs.get_data_by_destination())
    return sessions