from geojson import LineString
from math import radians, sin, cos

def get_test_geojson():
    center = (29.890902, 59.883984702)
    lines = []
    for i in range(8):
        angle = radians(i*45)
        length = 0.001
        to_point = (center[0] + length * sin(angle), center[1] + length * cos(angle))
        lines.append(LineString([center, to_point]))
    return lines