import log, re
from pymongo import MongoClient
from bson.objectid import ObjectId

mongo_client = MongoClient("mongodb://mongo:27017")
db = mongo_client.traffic

sessions = {
    "first": "5906827a5b530ee18ee4864c",
    "second": ""

}
def map_dict(f, dict):
    return {k: f(v) for k, v in dict.items()}

def get_clean_list(raw_data):
    clear_str = raw_data.replace(' ', '').replace('\n','').replace('\r', '')
    return list(filter(bool, re.findall(r"[\w']+", clear_str)))

def correct_session_specific(direction, session_id, point_number):
    result_direction = direction
    if session_id == sessions["first"]:
        if point_number == 2:
            pass

def valid_cat_data(cat_data):
    for direction in cat_data:
        if not re.match("^\d*[a-zA-Z]{2}$", direction):
            log.log_error(direction, "[ManualForm]Wrong direction")
            return False
    return True

def save_direction(direction, session_id, point_id, category):
    db.directions.insert_one({'direction': direction.upper(), 'session_id': ObjectId(session_id), 'point_id': point_id, 'category': category, 'datetime': None})

def save_cat_data(cat_name, cat_data, session_id, point_id):
    for direction in cat_data:
        match = re.match("^(\d*)([a-zA-Z]{2})$", direction)
        mult = match.group(1)
        dir = match.group(2)
        if mult:
            for i in range(int(mult)):
                save_direction(dir, session_id, point_id, cat_name)
        else:
            save_direction(dir, session_id, point_id, cat_name)

def process_manual_data(data):
    if db.directions.find_one({'session_id': ObjectId(data['session_id']), 'point_id':data['point_id']}):
        log.log_error("Data for session {} and point {} already exist!".format(data['session_id'], data['point_id']), "[ManualForm]")
        return False
    clean_cats = map_dict(get_clean_list, data['categories'])
    for cat_name, cat_data in clean_cats.items():
        if not valid_cat_data(cat_data):
            return False
    for cat_name, cat_data in clean_cats.items():
        save_cat_data(cat_name, cat_data, data['session_id'], data['point_id'])
    # log.log_debug(data)
    return True

if __name__ == '__main__':
    log.log_debug("ololo")
    # test_data1 = "AA,  B1, ag, \n ,fa"
    # print(process_manual_data(test_data1))