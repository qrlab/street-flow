from flask import Flask, request, render_template, jsonify
import os
import data as datalib
from forms.manual import ManualFlowDataForm
import log
from process import process_manual_data
import geo as geolib

app = Flask(__name__)
if 'FLASK_DEBUG' in os.environ and os.environ['FLASK_DEBUG'] =="True":
    app.debug = True

@app.route('/', methods=['GET', 'POST'])
def main():
    return render_template('home.html')

@app.route('/vis1')
def vis1():
    return render_template('vis1.html')

@app.route('/vis2')
def vis2():
    return render_template('vis2.html')

@app.route('/geo')
def geo():
    return jsonify(geolib.get_test_geojson())

@app.route('/data')
def data():
    return jsonify(get_data())

@app.route('/data-all-sessions')
def data_all_sessions():
    return jsonify(datalib.get_all_sessions(True))

@app.route('/manual', methods=['GET', 'POST'])
def manual():
    form = ManualFlowDataForm(request.form)
    form.session.choices = datalib.get_sessions_choices()
    if request.method == 'POST' and form.validate():
        form_data = {
            'session_id': form.session.data,
            'point_id': int(form.point.data),
            'categories': {
                'preschoolers': form.preschoolers.data,
                'y1017': form.y1017.data,
                'cadets': form.cadets.data,
                'students': form.students.data,
                'employeed': form.employeed.data,
                'parent_children': form.parent_children.data,
                'pensioners': form.pensioners.data
            }
        }

        if process_manual_data(form_data):
            log.log_info("Manual data sucessfully posted.", "[ManualDataForm]")
            return "Данные успешно отправлены!"
        return "Какая-то ошибка! Нажмите, \"назад\" в браузере и посмотрите, что может быть не так. Напишите в телеграм <a href=\"https://t.me/alateas\" target=\"_blank\">@alateas</a>"

    return render_template('manual-alt.html', form=form)

@app.route('/sessions/')
@app.route('/sessions/<session_id>')
def sessions(session_id=None):
    if not session_id:
        return render_template('sessions.html', sessions=datalib.get_all_sessions())
    log.log_debug(datalib.get_session_by_id(session_id).get_data_by_category())
    return render_template('session-data.html', session=datalib.get_session_by_id(session_id).get_data_by_category())

if __name__ == '__main__':
# a = get_raw_data()
    # print(list(get_random_raw_data()))
    app.run(host="0.0.0.0", port=80)