import logging
import collections
import telegram
from specific_config import config

notify_tg = True
recievers = [61834681, 175228927]

logger = logging.getLogger('core_debug')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(logging.Formatter('[%(asctime)s]*%(levelname)s*|%(desc)s|%(message)s'))
logger.addHandler(ch)

tg_bot = telegram.Bot(token=config['telegram_bot_token'])
def tostr(s):
    if isinstance(s, str):
        return s
    if isinstance(s, collections.Iterable) and not isinstance(s, dict):
        return list(s)
    return str(s)

def send_tg(msg):
    if notify_tg:
        tg_bot.sendMessage(chat_id=61834681, text=msg)

def log_info(val, desc=""):
    logger.info(tostr(val), extra={"desc":desc})
    send_tg("{} | ->{}".format(desc, tostr(val)))

def log_debug(val, desc=""):
    logger.debug(tostr(val), extra={"desc":desc})

def log_warn(val, desc=""):
    logger.warn(tostr(val), extra={"desc":desc})
    send_tg("{} | ->{}".format(desc, tostr(val)))

def log_error(val, desc=""):
    logger.error(tostr(val), extra={"desc":desc})
    send_tg("{} | ->{}".format(desc, tostr(val)))