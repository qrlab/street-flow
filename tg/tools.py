from functools import wraps
from settings import settings
from log import log_error

def admin_access(func):
    @wraps(func)
    def wrapped(bot, update, *args, **kwargs):
        # extract user_id from arbitrary update
        log_debug("yo")
        try:
            user_id = update.message.from_user.id
        except (NameError, AttributeError):
            try:
                user_id = update.inline_query.from_user.id
            except (NameError, AttributeError):
                try:
                    user_id = update.chosen_inline_result.from_user.id
                except (NameError, AttributeError):
                    try:
                        user_id = update.callback_query.from_user.id
                    except (NameError, AttributeError):
                        log_error("No user_id available in update.")
                        return
        log_debug(user_id, "user_id")
        log_debug(settings['admin_list'], "admin_list")
        if user_id not in settings['admins_list']:
            log_debug("Unauthorized access denied for {}.".format(chat_id))
            return
        return func(bot, update, *args, **kwargs)
    return wrapped