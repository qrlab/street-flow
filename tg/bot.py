from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from log import log_info, log_warn, log_debug, log_error
from specific_config import config
import traceback
from app import message_trigger
import telegram
from tools import admin_access
from settings import settings

def send_admins(bot, msg):
    try:
        for admin_id in settings['admins_list']:
            bot.sendMessage(chat_id=admin_id, text=msg)
    except:
        log_debug(traceback.format_exc(), "got_message exception")

# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    update.message.reply_text('Рад тебя видеть, это бот для сбора данных о потоках людей на улицах. Для более подробной иноформации отправь /help .')
    send_admins(bot, "{} {} pressed strart!".format(update.message.chat.first_name, update.message.chat.last_name))

def help(bot, update):
    update.message.reply_text("Это бот для сбора данных о потоках людей на улицах.\nПодробный мануал тут:\nКоманды:\n/keyboard тест клавиатуры")

def keyboard(bot, update):
    button1 = telegram.KeyboardButton(text="button1")
    button2 = telegram.KeyboardButton(text="/hide_keyboard")
    custom_keyboard = [[ button1, button2 ]]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard)
    update.message.reply_text('Вот тебе клавиатура.', reply_markup=reply_markup)

def hide_keyboard(bot, update):
    update.message.reply_text('Клавиатура отключена.', reply_markup=telegram.ReplyKeyboardHide())

@admin_access
def admin(bot, update):
    update.message.reply_text('Админский раздел.')

def got_message(bot, update):
    try:
        message_trigger(update)
    except:
        log_debug(traceback.format_exc(), "got_message exception")

def error(bot, update, error):
    log_warn('Update "{}" caused error "{}"'.format(update, error))


def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(config['telegram_bot_token'])

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("keyboard", keyboard))
    dp.add_handler(CommandHandler("hide_keyboard", hide_keyboard))
    dp.add_handler(CommandHandler("admin", admin))

    # on any text message - try to correct the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, got_message))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()

if __name__ == '__main__':
    main()
