from log import log_info, log_warn, log_debug, log_error
from pymongo import MongoClient

mongo_client = MongoClient("mongodb://mongo:27017")
db = mongo_client.traffic

def message_trigger(update):
    data = {"text": update.message.text, "date": update.message.date, "chat_id": update.message.chat_id}
    log_debug(update.message.chat_id, "New mesage from:")
    db.raw_messages.insert_one(data)
    update.message.reply_text(update.message.text)