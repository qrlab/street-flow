from pymongo import MongoClient
from datetime import datetime, timezone

mongo_client = MongoClient("mongodb://mongo:27017")
db = mongo_client.traffic

def get_dt(y, m, d, h, min):
    return datetime(y, m, d, h, min).astimezone(tz=timezone.utc)

result = db.point_sets.insert_one({'_id':1, 'desciption':'Петергоф.Квартариата.', 'points':[
        {
            '_id': 1,
            'description': 'Морской десант',
            'lat': 59.886263,
            'lon': 29.897250
        },
        {
            '_id': 2,
            'description': 'Эйхенская',
            'lat': 59.883141,
            'lon': 29.895337
        },
        {
            '_id': 3,
            'description': 'Пролетная',
            'lat': 59.883135,
            'lon': 29.897897
        },
        {
            '_id': 4,
            'description': 'Перекресток у Ракеты',
            'lat': 59.882014,
            'lon': 29.895332
        },
        {
            '_id': 5,
            'description': 'У стены',
            'lat': 59.879852,
            'lon': 29.894087
        }]
    })
point_set_id = result.inserted_id
db.sessions.insert_one({'number':1, 'point_set':point_set_id, 'date_start': get_dt(2017, 4, 8, 13, 0), 'date_finish': get_dt(2017, 4, 8, 14, 30)})
db.sessions.insert_one({'number':2, 'point_set':point_set_id, 'date_start': get_dt(2017, 4, 28, 8, 20), 'date_finish': get_dt(2017, 4, 28, 10, 0)})
result = db.sessions.insert_one({'number':3, 'point_set':point_set_id, 'date_start': get_dt(2017, 4, 28, 19, 25), 'date_finish': get_dt(2017, 4, 28, 20, 45)})

# db.directions.insert_one({'direction':'AB', 'session_id':result.inserted_id, 'point_id': 1, 'category': 'preschoolers', 'datetime': None})

print("Done.")